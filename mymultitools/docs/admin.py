from django.contrib import admin

from .models import StudentTicket, LibraryCard

# Register your models here.


@admin.register(StudentTicket)
class StudentTicketAdmin(admin.ModelAdmin):
    pass

@admin.register(LibraryCard)
class LibraryCardAdmin(admin.ModelAdmin):
    pass
