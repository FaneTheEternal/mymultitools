from django import forms
from django.shortcuts import get_object_or_404
from bootstrap_modal_forms.forms import BSModalForm

from .models import StudentTicket, LibraryCard
from users.models import Institute, Student


class CreateMixin(object):
    def set_init_data(self, **kwargs):
        self.student_data = get_object_or_404(
            Student, pk=kwargs.pop('student')
        )
        self.institute_data = self.student_data.institute
        return kwargs

    def setup_fields(self):
        self.fields['institute'].initial = self.institute_data
        self.fields['institute'].disabled = True

        self.fields['student'].initial = self.student_data
        self.fields['student'].disabled = True


class UpdateMixin(object):
    def setup_fields(self):
        self.fields['institute'].disabled = True
        self.fields['student'].disabled = True


class StudentTicketForm(CreateMixin, BSModalForm):
    class Meta:
        model = StudentTicket
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        kwargs = self.set_init_data(**kwargs)
        super().__init__(*args, **kwargs)
        self.setup_fields()


class LibraryCardtForm(CreateMixin, BSModalForm):
    class Meta:
        model = LibraryCard
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        kwargs = self.set_init_data(**kwargs)
        super().__init__(*args, **kwargs)
        self.setup_fields()


class StudentTicketUpdateForm(UpdateMixin, BSModalForm):
    class Meta:
        model = StudentTicket
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setup_fields()


class LibraryCardtUpdateForm(UpdateMixin, BSModalForm):
    class Meta:
        model = LibraryCard
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setup_fields()

