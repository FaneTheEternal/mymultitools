from django.conf.urls import url

from . import views


urlpatterns = [
    url(
        r'^student-ticket-create/(?P<pk>\d+)/$',
        views.StudentTicketCreateView.as_view(),
        name='create_student_ticket',
    ),
    url(
        r'^library-card-create/(?P<pk>\d+)/$',
        views.LibraryCardCreateView.as_view(),
        name='create_library_card',
    ),

    url(
        r'^student-ticket-update/(?P<pk>\d+)/$',
        views.StudentTicketUpdateView.as_view(),
        name='update_student_ticket',
    ),
    url(
        r'^library-card-update/(?P<pk>\d+)/$',
        views.LibraryCardUpdateView.as_view(),
        name='update_library_card',
    ),
    url(
        r'^test$',
        views.StudentTicketCreateTEST.as_view(),
        name='test_docs',
    )
]
