from django.db import models

from users.models import Student, Institute, UserProfile


class DocBase(models.Model):
    student = models.ForeignKey(
        Student,
        verbose_name='Студент',
        on_delete=models.CASCADE,
    )

    institute = models.ForeignKey(
        Institute,
        verbose_name='Учебное заведение',
        on_delete=models.CASCADE,
    )

    code = models.CharField(
        verbose_name='Код документа',
        max_length=50,
    )

    class Meta:
        abstract = True
        unique_together = [['student', 'institute']]


class StudentTicket(DocBase):
    def get_scan_path(instance, filename):
        return './{0}/StudentTicket/{1}'.format(
            instance.student.uuid,
            filename)

    scan = models.ImageField(
        verbose_name='Скан зачетки',
        upload_to=get_scan_path,
        height_field=None,
        width_field=None,
        max_length=None,
        null=True, blank=True,
    )

    def __str__(self):
        return 'Студенческий {0}'.format(self.student)

    class Meta(DocBase.Meta):
        abstract = False


class LibraryCard(DocBase):
    def get_scan_path_front(instance, filename):
        return './{0}/LibraryCard/front_{1}'.format(
            instance.student.uuid,
            filename)

    def get_scan_path_back(instance, filename):
        return './{0}/LibraryCard/back_{1}'.format(
            instance.student.uuid,
            filename)

    scan_front = models.ImageField(
        verbose_name='Скан спереди',
        upload_to=get_scan_path_front,
        height_field=None,
        width_field=None,
        max_length=None,
        null=True, blank=True,
    )

    scan_back = models.ImageField(
        verbose_name='Скан сзади',
        upload_to=get_scan_path_back,
        height_field=None,
        width_field=None,
        max_length=None,
        null=True, blank=True,
    )

    def __str__(self):
        return 'Читательский '.format(self.student)

    class Meta(DocBase.Meta):
        abstract = False


class Note(models.Model):
    owner = models.ForeignKey(
        UserProfile,
        verbose_name='Владелец',
        on_delete=models.CASCADE,
        related_name='notes'
    )
    name = models.CharField(
        'Название',
        max_length=50
    )
    text = models.TextField(
        'Текст',
        max_length=1000,
    )
