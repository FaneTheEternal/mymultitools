from django.urls import reverse_lazy
from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView
from django.views.generic import CreateView

from .forms import StudentTicketForm, LibraryCardtForm, StudentTicketUpdateForm, LibraryCardtUpdateForm
from .models import StudentTicket, LibraryCard


class DocsCreateBaseView(BSModalCreateView):
    template_name = 'modal_form_base.html'
    success_message = 'Success'
    modal_title = ''

    def get_success_url(self):
        return reverse_lazy(
            'student_detail',
            args=(self.object.study_group.pk,)
        )

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs['student'] = self.kwargs['pk']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modal_title'] = self.modal_title
        return context


class DocsUpdateBaseView(BSModalUpdateView):
    template_name = 'modal_form_base.html'
    success_message = 'Success'
    modal_title = ''

    def get_success_url(self):
        return reverse_lazy(
            'student_detail',
            args=(self.object.study_group.pk,)
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modal_title'] = self.modal_title
        return context


class StudentTicketCreateView(DocsCreateBaseView):
    form_class = StudentTicketForm
    modal_title = 'Добавить студенческий'


class LibraryCardCreateView(DocsCreateBaseView):
    form_class = LibraryCardtForm
    modal_title = 'Добавить читательский'


class StudentTicketUpdateView(DocsUpdateBaseView):
    form_class = StudentTicketUpdateForm
    modal_title = 'Изменить студенческий'
    model = StudentTicket


class LibraryCardUpdateView(DocsUpdateBaseView):
    form_class = LibraryCardtUpdateForm
    modal_title = 'Изменить читательский'
    model = LibraryCard


class StudentTicketCreateTEST(CreateView):
    model = StudentTicket
    template_name = 'docs/test.html'
    success_url = reverse_lazy('home')
    fields = '__all__'
