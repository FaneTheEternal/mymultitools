from django.urls import reverse_lazy, reverse
from django.shortcuts import get_object_or_404
from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView

from .models import Schedule, Subgroup
from .forms import SubgroupFrom, ScheduleForm

from users.models import StudyGroup


class BaseCreateMixin(object):
    template_name = 'modal_form_base.html'
    success_message = 'Success'
    title = ''

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modal_title'] = self.title
        return context


class SubgroupCreateView(BaseCreateMixin, BSModalCreateView):
    form_class = SubgroupFrom
    success_message = 'Success'
    title = 'Добавить расписание'

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        group = get_object_or_404(
            StudyGroup,
            pk=self.request.GET.get('group')
        )
        kwargs.update({
            'group': group,
        })
        return kwargs

    def get_success_url(self):
        return reverse_lazy(
            'study_group_detail',
            args=(self.object.group.pk,)
        )


class ScheduleCreateView(BaseCreateMixin, BSModalCreateView):
    form_class = ScheduleForm
    success_message = 'Success'
    title = 'Добавить пару'

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        group = get_object_or_404(
            Subgroup,
            pk=self.request.GET.get('group')
        )
        day = self.request.GET.get('day')
        is_even = self.request.GET.get('is_even')
        position = self.request.GET.get('position')
        kwargs.update({
            'group': group,
            'day': day,
            'is_even': is_even,
            'position': position,
        })
        return kwargs

    def get_success_url(self):
        return reverse_lazy(
            'study_group_detail',
            args=(self.object.group.group.pk,)
        )
