from django.db import models

from users.models import StudyGroup, Teacher


class Subgroup(models.Model):
    group = models.ForeignKey(
        StudyGroup,
        verbose_name='Группа',
        on_delete=models.CASCADE,
        related_name='subgroups'
    )
    name = models.CharField(
        'Название',
        max_length=50,
        null=True, blank=True,
    )

    def __str__(self):
        return '{0} {1}'.format(
            self.name or 'Подгруппа',
            self.group,
        )

    def get_schedule(self):
        qs = self.schedules.filter(replacement__isnull=True)
        schedule = {
            day: {
                pos: {
                        'even': qs.filter(day=day, position=pos, is_even=True).first(),
                        'not_even': qs.filter(day=day, position=pos, is_even=False).first(),
                    }
                for pos in range(1, Schedule.MAX_POSITIONS + 1)
            }
            for day, day_name in dict(Schedule.DAYS).items()
        }
        return schedule


class Schedule(models.Model):
    group = models.ForeignKey(
        Subgroup,
        verbose_name='Группа',
        on_delete=models.CASCADE,
        related_name='schedules'
    )
    teacher = models.ForeignKey(
        Teacher,
        verbose_name='Преподаватель',
        on_delete=models.CASCADE,
        related_name='schedules',
    )
    discipline = models.CharField(
        verbose_name='Дисциплина',
        max_length=40,
    )
    MONDAY = 'monday'
    TUESDAY = 'tuesday'
    WEDNESDAY = 'wednesday'
    THURSDAY = 'thursday'
    FRIDAY = 'friday'
    SATURDAY = 'saturday'
    SUNDAY = 'sunday'
    DAYS = (
        (MONDAY, 'Понедельник'),
        (TUESDAY, 'Вторник'),
        (WEDNESDAY, 'Среда'),
        (THURSDAY, 'Четверг'),
        (FRIDAY, 'Пятница'),
        (SATURDAY, 'Суббота'),
        (SUNDAY, 'Воскресенье'),
    )
    day = models.CharField(
        verbose_name='День недели',
        choices=DAYS,
        max_length=20,
    )
    is_even = models.BooleanField(
        verbose_name='Четная неделя',
    )
    MAX_POSITIONS = 9
    position = models.IntegerField(
        verbose_name='Номер пары',
        choices=((x, x) for x in range(1, MAX_POSITIONS + 1)),
    )
    LECTURE = 'lecture'
    PRACTICAL = 'practical'
    LABORATORY = 'laboratory'
    CONSULTATION = 'consultation'
    IWK = 'iwk'
    COURSE_WORK = 'coursework'
    PRACTICE_REPORT = 'practice_report'
    RESEARCH_WORK = 'research_work'
    TEST = 'test'
    EXAM = 'exam'
    TYPES = (
        ('', ''),
        (LECTURE, 'Лекция'),
        (PRACTICAL, 'Практическая работа'),
        (LABORATORY, 'Лабораторная работа'),
        (CONSULTATION, 'Консультация'),
        (IWK, 'Контроль самостоятельной работы'),
        (COURSE_WORK, 'Защита курсовой работы'),
        (PRACTICE_REPORT, 'Защита отчета по практике'),
        (RESEARCH_WORK, 'Научно исследовательская работа'),
        (TEST, 'Зачет'),
        (EXAM, 'Экзамен'),
    )
    kind = models.CharField(
        verbose_name='Вид пары',
        choices=TYPES,
        max_length=40,
    )
    audience = models.CharField(
        verbose_name='Аудиотория',
        max_length=20,
        blank=True, null=True,
    )
    replacement = models.DateField(
        verbose_name='Дата замены',
        auto_now=False,
        auto_now_add=False,
        null=True, blank=True,
    )

    def __str__(self):
        return '{}, {}'.format(
            self.discipline,
            dict(self.TYPES).get(self.kind),
        )
