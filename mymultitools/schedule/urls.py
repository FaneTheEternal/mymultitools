from django.conf.urls import url

from . import views


urlpatterns = [
    url(
        r'^subgroup-create/$',
        views.SubgroupCreateView.as_view(),
        name='create_subgroup',
    ),
    url(
        r'^schedule-create/$',
        views.ScheduleCreateView.as_view(),
        name='create_schedule',
    )
]
