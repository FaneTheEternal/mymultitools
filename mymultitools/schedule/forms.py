from django import forms
from django.shortcuts import get_object_or_404
from bootstrap_modal_forms.forms import BSModalForm

from .models import Schedule, Subgroup
from users.models import Teacher, StudyGroup


class SubgroupFrom(BSModalForm):
    class Meta:
        model = Subgroup
        fields = ['name', 'group']

    def __init__(self, **kwargs):
        group = kwargs.pop('group')
        super().__init__(**kwargs)
        self.fields['group'].initial = group
        self.fields['group'].disabled = True


class ScheduleForm(BSModalForm):
    class Meta:
        model = Schedule
        exclude = ['replacement']

    def __init__(self, **kwargs):
        group = kwargs.pop('group')
        day = kwargs.pop('day')
        is_even = kwargs.pop('is_even')
        position = kwargs.pop('position')
        super().__init__(**kwargs)
        self.fields['group'].initial = group
        self.fields['group'].disabled = True
        self.fields['day'].initial = day
        self.fields['day'].disabled = True
        self.fields['is_even'].initial = is_even
        self.fields['is_even'].disabled = True
        self.fields['position'].initial = position
        self.fields['position'].disabled = True
