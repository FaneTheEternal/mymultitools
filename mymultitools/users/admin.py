from django.contrib import admin

from .models import UserProfile, Student, StudyGroup, Teacher, InstituteAgent, Institute


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    pass


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    pass


@admin.register(StudyGroup)
class StudyGroupAdmin(admin.ModelAdmin):
    pass


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    pass


@admin.register(InstituteAgent)
class InstituteAgentAdmin(admin.ModelAdmin):
    pass


@admin.register(Institute)
class InstituteAdmin(admin.ModelAdmin):
    pass
