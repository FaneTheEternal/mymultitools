from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'^$',
        views.HomeView.as_view(),
        name='home',
    ),
    url(
        r'^agent_get$',
        views.InstituteAgentGetView.as_view(),
        name='get_agent',
    ),
    url(
        r'^student_get$',
        views.StudentGetView.as_view(),
        name='get_student',
    ),
    url(
        r'^teacher_get$',
        views.TeacherFormGetView.as_view(),
        name='get_teacher',
    )
]

urlpatterns += [
    url(
        r'^institute/(?P<pk>\d+)/$',
        views.InstituteDetailView.as_view(),
        name='institute_detail',
    ),
    url(
        r'^study-group/(?P<pk>\d+)/$',
        views.StudyGroupDetailView.as_view(),
        name='study_group_detail',
    ),
    url(
        r'^student/(?P<pk>\d+)/$',
        views.StudentDetailView.as_view(),
        name='student_detail'
    ),
    url(
        r'^teacher/(?P<pk>\d+)/$',
        views.TeacherDetailView.as_view(),
        name='teacher_detail'
    ),
]

urlpatterns += [
    url(
        r'^student-create/(?P<pk>\d+)/$',
        views.StudentCreateView.as_view(),
        name='create_student',
    ),
    url(
        r'^group-create/(?P<pk>\d+)/$',
        views.StudyGroupCreateView.as_view(),
        name='create_group',
    ),
    url(
        r'^teacher-create/(?P<pk>\d+)/$',
        views.TeacherCreateView.as_view(),
        name='create_teacher',
    ),
    url(
        r'^agent-create/$',
        views.InstituteAgentCreateView.as_view(),
        name='create_agent',
    ),
    url(
        r'^institute-create/$',
        views.InstituteCreateView.as_view(),
        name='create_institute',
    ),
]
