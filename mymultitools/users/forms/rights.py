from ..models import InstituteAgent, Student, Teacher, Institute

from django import forms
from bootstrap_modal_forms.forms import BSModalForm


class BaseGetForm(BSModalForm):
    get_model = None
    uuid = forms.UUIDField(
        label='Ключ',
        required=True,
        error_messages={'invalid': 'Проверте правильность ключа'}
    )

    class Meta:
        model = None
        fields = ['uuid']

    def clean(self):
        cleaned_data = super().clean()
        uuid = cleaned_data.get('uuid')
        obj = self.get_model.objects.filter(uuid=uuid).first()
        if not obj:
            raise forms.ValidationError('Проверте правильность ключа')
        if obj.user_profile and obj.user_profile != self.request.user.profile:
            raise forms.ValidationError('Этот ключ уже использовали')
        return cleaned_data

    def save(self, commit=True):
        instance = self.get_model.objects.get(uuid=self.cleaned_data['uuid'])
        instance.user_profile = self.request.user.profile
        if commit:
            instance.save()
        return instance


class InstituteAgentGetForm(BaseGetForm):
    get_model = InstituteAgent

    class Meta(BaseGetForm.Meta):
        model = InstituteAgent


class StudentGetForm(BaseGetForm):
    get_model = Student

    class Meta(BaseGetForm.Meta):
        model = Student


class TeacherGetForm(BaseGetForm):
    get_model = Teacher

    class Meta(BaseGetForm.Meta):
        model = Teacher


class InstituteAgentCreateForm(BSModalForm):
    class Meta:
        model = InstituteAgent
        fields = ['institute', 'uuid']


class InstituteCreateForm(BSModalForm):
    class Meta:
        model = Institute
        fields = '__all__'
