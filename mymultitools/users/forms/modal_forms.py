from django import forms
from django.shortcuts import get_object_or_404
from bootstrap_modal_forms.forms import BSModalForm

from ..models import Student, Teacher, StudyGroup, Institute


class StudentForm(BSModalForm):
    class Meta:
        model = Student
        fields = ['study_group']
        widgets = {'study_group': forms.HiddenInput}

    def __init__(self, *args, **kwargs):
        study_group = get_object_or_404(
            StudyGroup, pk=kwargs.pop('study_group')
        )
        super().__init__(*args, **kwargs)
        self.fields['study_group'].initial = study_group
        self.fields['study_group'].disabled = True
        self.fields['study_group'].label = ''


class TeacherForm(BSModalForm):
    class Meta:
        model = Teacher
        fields = ['institute']
        widgets = {'institute': forms.HiddenInput}

    def __init__(self, *args, **kwargs):
        institute = get_object_or_404(
            Institute, pk=kwargs.pop('institute')
        )
        super().__init__(*args, **kwargs)
        self.fields['institute'].initial = institute
        self.fields['institute'].disabled = True
        self.fields['institute'].label = ''


class StudyGroupForm(BSModalForm):
    class Meta:
        model = StudyGroup
        fields = ['institute', 'name']
        widgets = {'institute': forms.HiddenInput}

    def __init__(self, *args, **kwargs):
        institute = get_object_or_404(
            Institute, pk=kwargs.pop('institute')
        )
        super().__init__(*args, **kwargs)
        self.fields['institute'].initial = institute
        self.fields['institute'].disabled = True
        self.fields['institute'].label = ''
