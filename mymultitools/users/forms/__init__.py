from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

from .rights import InstituteAgentGetForm, StudentGetForm, TeacherGetForm, InstituteAgentCreateForm, InstituteCreateForm
from .modal_forms import StudentForm, StudyGroupForm, TeacherForm
from ..models import StudyGroup


class SignUpForm(UserCreationForm):
    name = forms.CharField(
        max_length=100,
        help_text='Имя',
        required=True,
    )
    surname = forms.CharField(
        max_length=100,
        help_text='Фамилия',
        required=True,
    )
    middle_name = forms.CharField(
        max_length=100,
        help_text='Отчество',
        required=False,
    )
    email = forms.EmailField(
        max_length=150,
        help_text='Email',
        required=True,
    )

    class Meta:
        model = User
        fields = (
            'username', 'name', 'surname', 'middle_name',
            'email', 'password1', 'password2',
        )
