from django.db import models
from .user import UserProfile
from .institute import Institute

import uuid


class Teacher(models.Model):
    user_profile = models.ForeignKey(
        UserProfile,
        verbose_name='Профиль',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='teachers',
    )
    institute = models.ForeignKey(
        Institute,
        verbose_name='Учебное заведение',
        on_delete=models.CASCADE,
        related_name='teachers',
    )
    uuid = models.UUIDField(
        'Индефикатор учетки преподавателя',
        default=uuid.uuid4,
    )

    def __str__(self):
        return '{0} {1}'.format(
            self.user_profile or 'Не назначен',
            self.institute,
        )
