from django.db import models
from .institute import Institute
from .user import UserProfile

import uuid


class StudyGroup(models.Model):
    institute = models.ForeignKey(
        Institute,
        verbose_name='Учебное заведение',
        on_delete=models.CASCADE,
        related_name='study_groups',
    )
    name = models.CharField(
        'Название группы',
        max_length=20
    )

    def __str__(self):
        return self.name


class Student(models.Model):
    user_profile = models.ForeignKey(
        UserProfile,
        verbose_name='Профиль',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='students',
    )
    study_group = models.ForeignKey(
        StudyGroup,
        verbose_name='Группа',
        on_delete=models.CASCADE,
        related_name='students',
    )
    is_headman = models.BooleanField('Староста', default=False)
    uuid = models.UUIDField(
        'Индефикатор учетки студента',
        default=uuid.uuid4,
    )

    def __str__(self):
        return '[{0}] {1}'.format(
            self.study_group,
            self.user_profile or 'Ничей'
        )

    @property
    def institute(self):
        return self.study_group.institute
