import uuid

from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save


class UserProfile(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='profile'
    )

    name = models.CharField(
        verbose_name='Имя',
        max_length=30,
    )
    surname = models.CharField(
        verbose_name='Фамилия',
        max_length=30,
    )
    middle_name = models.CharField(
        verbose_name='Отчество',
        max_length=30,
        null=True, blank=True,
    )
    email = models.EmailField(
        max_length=150
    )

    uuid = models.UUIDField(
        default=uuid.uuid4
    )

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            if instance.is_superuser:
                UserProfile.objects.create(user=instance, email='exapmle@mail.ru')
            else:
                UserProfile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_userprofile(sender, instance, **kwargs):
        instance.profile.save()

    def __str__(self):
        return '[{0}] {1} {2} {3}'.format(
            self.user.username,
            self.name,
            self.surname,
            self.middle_name,
        )
