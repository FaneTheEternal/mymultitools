from django.db import models
from .user import UserProfile

import uuid


class Institute(models.Model):
    name = models.CharField(
        verbose_name='Название учебного заведения',
        max_length=100,
    )

    def __str__(self):
        return self.name


class InstituteAgent(models.Model):
    user_profile = models.ForeignKey(
        UserProfile,
        verbose_name='Профиль',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='institute_agents',
    )
    institute = models.ForeignKey(
        Institute,
        verbose_name='Учебное заведение',
        on_delete=models.CASCADE,
        related_name='institute_agents',
    )
    uuid = models.UUIDField(
        'Индефикатор учетки представителя учебного заведения',
        default=uuid.uuid4,
    )

    def __str__(self):
        return 'Агент {0}'.format(self.institute)

    def get_cls(self):
        return InstituteAgent
