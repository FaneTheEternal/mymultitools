from django.urls import reverse_lazy
from bootstrap_modal_forms import generic

from ..forms import StudentForm, StudyGroupForm, TeacherForm, InstituteCreateForm
from ..models import Institute

class StudentCreateView(generic.BSModalCreateView):
    template_name = 'modal_forms/create_student.html'
    form_class = StudentForm
    success_message = 'Success'

    def get_success_url(self):
        return reverse_lazy(
            'study_group_detail',
            args=(self.object.study_group.pk,)
        )

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs['study_group'] = self.kwargs['pk']
        return kwargs


class StudyGroupCreateView(generic.BSModalCreateView):
    template_name = 'modal_forms/create_study_group.html'
    form_class = StudyGroupForm
    success_message = 'Success'

    def get_success_url(self):
        return reverse_lazy(
            'institute_detail',
            args=(self.object.institute.pk,)
        )

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs['institute'] = self.kwargs['pk']
        return kwargs


class TeacherCreateView(generic.BSModalCreateView):
    template_name = 'modal_forms/create_teacher.html'
    form_class = TeacherForm
    success_message = 'Success'

    def get_success_url(self):
        return reverse_lazy(
            'institute_detail',
            args=(self.object.institute.pk,)
        )

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs['institute'] = self.kwargs['pk']
        return kwargs


class InstituteCreateView(generic.BSModalCreateView):
    template_name = 'modal_form_base.html'
    form_class = InstituteCreateForm
    success_message = 'Success'
    success_url = reverse_lazy('home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modal_title'] = 'Добавить уч заведение'
        return context
