from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.core.exceptions import PermissionDenied

from .users import signup_view
from .rights import InstituteAgentGetView, StudentGetView, TeacherFormGetView, InstituteAgentCreateView
from .modal_views import StudentCreateView, StudyGroupCreateView, TeacherCreateView, InstituteCreateView
from ..models import StudyGroup, Institute, Student, Teacher, InstituteAgent
from ..forms import StudyGroupForm
from schedule.models import Schedule


class HomeView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'users/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['institute_agents'] = InstituteAgent.objects.all()
        return context


class InstituteDetailView(generic.DetailView):
    model = Institute
    template_name = 'users/institute_detail.html'


class StudyGroupDetailView(generic.DetailView):
    model = StudyGroup
    template_name = 'users/study_group_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['week_days'] = dict(Schedule.DAYS)
        return context


class StudentDetailView(generic.DetailView):
    model = Student
    template_name = 'users/student_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['week_days'] = dict(Schedule.DAYS)
        return context


class TeacherDetailView(generic.DeleteView):
    model = Teacher
    template_name = 'users/teacher_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['week_days'] = dict(Schedule.DAYS)
        return context
