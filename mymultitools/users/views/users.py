from django.views import generic
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect

from ..forms import SignUpForm


def signup_view(request):
    form = SignUpForm(request.POST)
    if form.is_valid():
        user = form.save()
        user.refresh_from_db()

        user.profile.name = form.cleaned_data.get('name')
        user.profile.surname = form.cleaned_data.get('surname')
        user.profile.middle_name = form.cleaned_data.get('middle_name')
        user.profile.email = form.cleaned_data.get('email')

        user.save()
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        user = authenticate(username=username, password=password)
        login(request, user)
        return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})
