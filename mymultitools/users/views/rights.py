from django.urls import reverse_lazy
from bootstrap_modal_forms import generic

from ..forms import InstituteAgentGetForm, StudentGetForm, TeacherGetForm, InstituteAgentCreateForm
from ..models import InstituteAgent


class InstituteAgentGetView(generic.BSModalCreateView):
    template_name = 'modal_forms/institute_agent_form.html'
    form_class = InstituteAgentGetForm
    success_message = 'Success'
    success_url = reverse_lazy('home')


class StudentGetView(generic.BSModalCreateView):
    template_name = 'modal_forms/student_form.html'
    form_class = StudentGetForm
    success_message = 'Success'
    success_url = reverse_lazy('home')


class TeacherFormGetView(generic.BSModalCreateView):
    template_name = 'modal_forms/teacher_form.html'
    form_class = TeacherGetForm
    success_message = 'Success'
    success_url = reverse_lazy('home')


class InstituteAgentCreateView(generic.BSModalCreateView):
    template_name = 'modal_form_base.html'
    form_class = InstituteAgentCreateForm
    success_message = 'Success'
    success_url = reverse_lazy('home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modal_title'] = 'Добавить представителя'
        return context
