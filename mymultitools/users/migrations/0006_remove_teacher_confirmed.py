# Generated by Django 3.0.5 on 2020-05-03 01:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20200502_1922'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='teacher',
            name='confirmed',
        ),
    ]
