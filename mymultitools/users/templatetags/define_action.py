from django import template
register = template.Library()


@register.filter
def lookup(dist, key):
    return dist.get(key)


@register.filter
def get_all(obj):
    return obj.__class__.objects.all()
