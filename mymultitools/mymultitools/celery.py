import os
import django

from celery import Celery
from celery.schedules import crontab
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mymultitools.settings')

django.setup()

app = Celery('mymultitools')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'run-every-single-minute': {
        'task': 'mymultitools.tasks.task_update_uuid',
        'schedule': crontab(),
    },
}
