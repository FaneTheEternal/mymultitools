from celery import shared_task

from users.models import UserProfile
from .services import queryset_foreach, update_uuid


@shared_task
def task_update_uuid():
    print('update_uuid')
    qs = UserProfile.objects.all()
    queryset_foreach(qs, update_uuid)
