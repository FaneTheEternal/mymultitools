import uuid

from .qs import queryset_foreach


def update_uuid(record):
    print(record)
    record.uuid = uuid.uuid4()
    record.save()
