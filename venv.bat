@echo off

if not exist "virtualenv" python -m venv virtualenv
if errorlevel 1 goto ending

virtualenv\Scripts\activate

:ending